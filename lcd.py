#FeedyPi
#author : Alessio Trivisonno
#email : alessio.trivisonno@yahoo.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python2
import RPi.GPIO as GPIO
import lcddriver
from time import *
import os
import sys
import signal
from myfunctions import *
import threading

GPIO.setmode(GPIO.BCM)
#led blinking
led = 19
GPIO.setup(led,GPIO.OUT)
#button skip info
button = 26
GPIO.setup(button,GPIO.IN)

lcd = lcddriver.lcd()
rfPath = "/home/fox/test-python/lcd/.p1"
pid_file = "/home/fox/test-python/lcd/pid" 

pid = open(pid_file,"w+")
pid.write(str(os.getpid()))
pid.close()

first_line_priority = threading.Semaphore()
second_line_priority = threading.Semaphore()

try:

    os.mkfifo(rfPath)

except OSError:
    pass


def terminate (x,y):
    """terminates the program cleanly"""

    
    first_line_priority.acquire(blocking=True)
    second_line_priority.acquire(blocking=True)
    lcd.lcd_clear()
    lcd.lcd_display_string("closing",1)
    sleep(1)
    lcd.lcd_clear()
    #acquire semaphore so no one tries to write to the lcd
    lcd.sem.acquire()
    lcd.lcd_device.write_cmd(1 | 0x00) 
    os.remove(pid_file)
    GPIO.cleanup()
    sys.exit(0)

def info_diplay():
    """displays the informations in myfunctions"""
    count = 1
    line = 2
    while True:

#it is not recommended to make the following blocking as whenever the release occurs the system gives the priority to a RANDOM thread who's 'waiting 
        if (second_line_priority.acquire(blocking=False) == True ):
            # print("info: acquired")
            # second_line_priority.release()
            info = randomize(count % 4)
            lcd.lcd_display_string("                ",line)
            lcd.lcd_display_string(info, line)
#this is used to enable the button to skip to the next info
            second_line_priority.release()
            channel = GPIO.wait_for_edge(button, GPIO.FALLING, timeout=10000,bouncetime=300)
            count = count + 1
            sleep (1)
        else:
            # print("info: target busy, sleeping 6 sec")
            sleep(6)



def updatetime():
    """thread intended to update the time"""

    while True:
        
        string = date()
        dot = string

#it is not recommended to make the following blocking as whenever the release occurs the system gives the priority to a RANDOM thread who's 'waiting 
        if (first_line_priority.acquire(blocking=False) == True ):
            lcd.lcd_display_string(dot, 1)
#add led while here
            count = 0
            first_line_priority.release()
            while (count < 8):
                GPIO.output(led, GPIO.HIGH)
                sleep (1)
                GPIO.output(led, GPIO.LOW)
                sleep (1)
                count += 2
        else:
            # print("time: target busy, sleeping 6 sec")
            sleep(6)
            count = count + 6

#capture signals. Needed to close the program politely
signal.signal(signal.SIGTERM, terminate)
signal.signal(signal.SIGINT, terminate)



#greeting message
lcd.lcd_display_string("Hello!",1)
sleep (2)
lcd.lcd_clear()



#starts threads for clock and info messages
t = threading.Thread (target=updatetime)
t.daemon = True
t.start()

pt = threading.Thread(target=info_diplay)
pt.daemon = True
pt.start()


#this is used to semaphore handling purposes. It is needed since acquiring the priority during the timer is necessary only if it
#if the first time. Maybe a better solution is possible
timer_going = False

while True:

#DEBUG
    # while (True):
        # eval(raw_input("shell: "))
    rp = open("/home/fox/test-python/lcd/.p1", 'r')
    message = rp.read()
    print(message.rstrip())
    rp.close()

#to handle display turn off
    if (message.rstrip() == "0"):
        first_line_priority.acquire()
        second_line_priority.acquire()
        lcd.lcd_clear()
        lcd.lcd_display_string("turning off",1)
        sleep (2)
        lcd.lcd_clear()
        lcd.lcd_device.write_cmd(1 | 0x00) 

#handle the motion lamp plugin
    elif (message.rstrip()[:4] == "LAMP"):
#acquire the priority only the firt timer the timer is triggered
        if (timer_going == False):
            second_line_priority.acquire()
            timer_going = True
        timer = float( message.rstrip()[4:6])
        # print("timer",timer)
        string = "Timer: " + str(int(timer)) + "sec"
        lcd.lcd_display_string("                     ",2)
        lcd.lcd_display_string(string,2)
        if (int(timer) <= 1):
            second_line_priority.release()
            timer_going = False
            # print("timer has expired")

    else:
#if too long split on both lines
        if (len(message.rstrip()) > 16):
            first_line_priority.acquire()
            second_line_priority.acquire()
            lcd.lcd_clear()
            lcd.lcd_display_string(message.rstrip()[:16],1)
            lcd.lcd_display_string(message.rstrip()[17:],2)
            sleep(10)
            lcd.lcd_clear()
            first_line_priority.release()
            second_line_priority.release()
        else:
#otherwise keep the clock going and display the message in the second line only
            second_line_priority.acquire()
            lcd.lcd_display_string("                ",2)
            lcd.lcd_display_string(message.rstrip(),2)
            sleep(10)
            lcd.lcd_clear()
            second_line_priority.release()
