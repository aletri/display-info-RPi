import datetime
import psutil
import subprocess



def date ():
    '''returns a string containing a formatted current time and date'''

    monts = {
            1 : "GEN",
            2 : "FEB",
            3 : "MAR",
            4 : "APR",
            5 : "MAY",
            6 : "JUN",
            7 : "JUL",
            8 : "AUG",
            9 : "SEP",
            10 : "OCT",
            11 : "NOV",
            12 : "DIC",
            }
    days = {
            0 : "MO",
            1 : "TU",
            2 : "WE",
            3 : "TH",
            4 : "FR",
            5 : "SA",
            6 : "SU",
            }
    now = datetime.datetime.now()
    day = days[now.weekday()]
    curr_month = monts[now.month]
    time = str(now.hour) +":"+ str (now.minute) + "  " + day + " " + str(now.day)+ " " + curr_month  
#adding zeros where needed for better view
    hour = now.hour
    minute = now.minute
    if ( hour < 10 ):
        hour = "0" + str(hour)
    else:
        hour = str(hour)
    if ( minute < 10 ):
        minute = "0" + str(minute)
    else:
        minute = str(minute)


    time = hour +":"+ minute + "  " + day + " " + str(now.day)+ " " + curr_month  

    return time


def load():
    '''returns a string with the percentage of cpu and memory used'''

    percentage = str(psutil.cpu_percent(interval=1))
    free = str(psutil.virtual_memory()[2])
        
    string = "CPU=" + percentage + "%" + "MEM=" + free + "%"

    return string

def disk_usage():
    '''return a string with the percentage of disk used'''
    usage = psutil.disk_usage('/')[3]
    string = "DISK=" + str(usage) + "%"
    return string

def boot_time():
    '''return a string with the boot time'''
    proc = subprocess.Popen(['uptime' , "-p"],stdout=subprocess.PIPE)
    line = proc.stdout.readline()
    string = line.rstrip()
    string = string.replace("hours","H_")
    string = string.replace("hour","H_")
    string = string.replace("minutes","M")
    string = string.replace("minute","M")
    string = string.replace("days","D")
    string = string.replace("day","D_")
    string = string.replace(",","")
    string = string.replace(" ","")
    string = string.replace("up","UP=")
    if line != '':
        return string

def temp ():
    '''returns a string with the outside and inside temperature'''
    proc = subprocess.Popen(['/home/fox/test-python/lcd/sensor'],stdout=subprocess.PIPE)
    line = proc.stdout.readline()
    if line != '':
        string = line.rstrip()
    else:
        string = temp()

    return string
        

functions = {
        0 : boot_time,
        1 : load,
        2 : disk_usage,
        3 : temp,
        }

def randomize (n):
    '''cycle beetween the informations to be displayed'''
    return (functions[n]())

